exports.config =
  # See http://brunch.io/#documentation for docs.

  paths:
    public: './build'
    watched: [
      'app',
      'assets',
      'styles',
      'vendor'
    ]

  modules:
    definition: false
    wrapper: false

  files:
    javascripts:
      joinTo:
        'app/Game.js': /^app/
        'app/vendor.js': /^vendor/
      order:
        before: [
          'app/foundation/Scene.coffee'
          'app/managers/GameManager.coffee'
        ]
        after: [
          'app/scenes/ExampleScene.coffee'
          'app/Game.coffee'
        ]

    stylesheets:
      joinTo:
        'styles/stylesheets/screen.css'

    templates:
      joinTo:
        'app.js'
