class GameManager
  constructor: (screenWidth, screenHeight, rootSceneType) ->
    # Game setup
    @game = new Phaser.Game(screenWidth, screenHeight, Phaser.CANVAS, '', { preload: @preload, create: @start, update: @update, render: @render })
    @currentScene = if rootSceneType? then new rootSceneType(@game) else null

  preload: () =>
    # Preload assets here
    @game.load.image('logo', '/img/phaser.png')

  start: () =>
    # Initialisation here
    if @currentScene?
      @currentScene.start()

  update: () =>
    # Update loop
    if @currentScene?
      @currentScene.update()

  render: () =>
    # Render loop
    if @currentScene?
      @currentScene.render()

  showSceneType: (sceneType) ->
    if sceneType?
      if @currentScene?
        @currentScene.destroy()
      @currentScene = new sceneType(@game)
      @currentScene.start()
