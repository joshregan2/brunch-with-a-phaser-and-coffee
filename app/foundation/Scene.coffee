class Scene
  constructor: (game) ->
    # Initialise scene
    @game = game

  start: () ->
    # Perform start up logic here

  destroy: () ->
    # Call this to destroy this and any references

  update: () ->
    # Update loop

  render: () ->
    # Render loop
