var Scene;

Scene = (function() {
  function Scene(game) {
    this.game = game;
  }

  Scene.prototype.start = function() {};

  Scene.prototype.destroy = function() {};

  Scene.prototype.update = function() {};

  Scene.prototype.render = function() {};

  return Scene;

})();
;var GameManager,
  __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

GameManager = (function() {
  function GameManager(screenWidth, screenHeight, rootSceneType) {
    this.render = __bind(this.render, this);
    this.update = __bind(this.update, this);
    this.start = __bind(this.start, this);
    this.preload = __bind(this.preload, this);
    this.game = new Phaser.Game(screenWidth, screenHeight, Phaser.CANVAS, '', {
      preload: this.preload,
      create: this.start,
      update: this.update,
      render: this.render
    });
    this.currentScene = rootSceneType != null ? new rootSceneType(this.game) : null;
  }

  GameManager.prototype.preload = function() {
    return this.game.load.image('logo', '/img/phaser.png');
  };

  GameManager.prototype.start = function() {
    if (this.currentScene != null) {
      return this.currentScene.start();
    }
  };

  GameManager.prototype.update = function() {
    if (this.currentScene != null) {
      return this.currentScene.update();
    }
  };

  GameManager.prototype.render = function() {
    if (this.currentScene != null) {
      return this.currentScene.render();
    }
  };

  GameManager.prototype.showSceneType = function(sceneType) {
    if (sceneType != null) {
      if (this.currentScene != null) {
        this.currentScene.destroy();
      }
      this.currentScene = new sceneType(this.game);
      return this.currentScene.start();
    }
  };

  return GameManager;

})();
;var ExampleScene,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

ExampleScene = (function(_super) {
  __extends(ExampleScene, _super);

  function ExampleScene(game) {
    ExampleScene.__super__.constructor.call(this, game);
    this.logoSprite = null;
  }

  ExampleScene.prototype.start = function() {
    ExampleScene.__super__.start.call(this);
    this.logoSprite = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'logo');
    return this.logoSprite.anchor.setTo(0.5, 0.5);
  };

  ExampleScene.prototype.destroy = function() {
    ExampleScene.__super__.destroy.call(this);
    return this.logoSprite.destroy(true);
  };

  ExampleScene.prototype.update = function() {
    return ExampleScene.__super__.update.call(this);
  };

  ExampleScene.prototype.render = function() {
    return ExampleScene.__super__.render.call(this);
  };

  return ExampleScene;

})(Scene);
;window.SharedGameManager = new GameManager(800, 600, ExampleScene);
;
//# sourceMappingURL=Game.js.map